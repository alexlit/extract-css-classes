//jQuery noconflict
$.noConflict();

jQuery(document).ready(function ($) {


    /*
    |--------------------------------------------------------------------------
    | toggleAttr() jQuery plugin
    |--------------------------------------------------------------------------
    |
    | Добавление метода toggleAttr()
    |
    */

    $.fn.toggleAttr = function (attr, value) {
        return this.each(function () {
            var $this = $(this);
            $this.attr(attr) ? $this.removeAttr(attr) : $this.attr(attr, value);
        });
    };

    /*
    |--------------------------------------------------------------------------
    | BOWSER
    |--------------------------------------------------------------------------
    |
    | Определение браузера пользователя
    |
    */

    // $('html').attr('data-browser', bowser.name.toLowerCase().replace(/\s/g, '-'));
    // $('html').attr('data-browser-version', Math.floor(bowser.version));


    /*
    |--------------------------------------------------------------------------
    | IE FIX
    |--------------------------------------------------------------------------
    |
    | Фикс css свойства 'object-fit' для IE и Edge браузеров
    |
    */

    if ($('html[data-browser=internet-explorer]').length || $('html[data-browser=microsoft-edge]').length) {
        $('img[data-fix--ie--object-fit]').each(function () {
            $(this).css({
                    'backgroundImage': 'url(' + $(this).attr('src') + ')',
                    'backgroundPosition': 'center center',
                    'backgroundRepeat': 'no-repeat',
                    'backgroundSize': 'cover',
                    'display': 'block'
                })
                .removeAttr('src');
            console.log($(this).css('objectPosition'));
            console.log($(this).css('objectFit'));
        });
    };

});