jQuery(document).ready(function($) {
  /*
    |--------------------------------------------------------------------------
    | CSS CLASS EXTRACT
    |--------------------------------------------------------------------------
    |
    | Конфигурация экспорта css классов из верстки
    |
    */

  if ($('.css-extract').length) {
    // конфигурация ace.js

    var inputField = ace.edit('css-extract-input');
    var outputField = ace.edit('css-extract-output');

    inputField.$blockScrolling = Infinity;
    outputField.$blockScrolling = Infinity;

    inputField.setShowPrintMargin(false);
    inputField.setTheme('ace/theme/tm');
    inputField.setFontSize(14);
    inputField.getSession().setMode('ace/mode/html');
    inputField.setValue(
      '<header class="header">\n\t<hgroup class="header__content-wrap">\n\t\t<h1 class="header__title">Привет! Этот сервис поможет извлечь css классы из верстки</h1>\n\t\t<p class="header__subtitle">Извлекаются только классы без каскадов, тэгов, идентификаторов и прочих ненужных вещей</p>\n\t</hgroup>\n</header>'
    );
    inputField.getSession().setUseWrapMode(true);

    outputField.setTheme('ace/theme/tm');
    outputField.setFontSize(14);
    outputField.getSession().setMode('ace/mode/css');
    outputField.renderer.setShowGutter(false);

    // конфигурация autoclasscss.js
    var extractCssClasses = new Autoclasscss({
      brace: 'default',
      flat: true,
      ignore: false,
      indent: ['spaces', 4],
      inner: false,
      line: false,
      tag: false
    });

    // настройка извлечения
    $('#css-extract-input').on('keyup', function() {
      // получаем данные из кода загруженного пользователем
      extractCssClasses.set(inputField.getValue());

      // экспортируем css классы
      outputField.setValue(extractCssClasses.get());
      outputField.gotoLine(1);

      // сортировка по алфавиту
      //outputField.sortLines();
    });

    // после загрузки страницы экспортируем демо-классы и убираем подсветку редатора
    $('#css-extract-input').trigger('keyup');
    inputField.gotoLine(1);
    outputField.gotoLine(1);
  }
});
